class BreedsController < ApplicationController

  def index
    @breed = DogBreedFetcher.fetch
  end

  def list
    @breeds = DogBreedFetcher.list
  end

  def show
    @breed = DogBreedFetcher.fetch(params[:id])
  end

end
